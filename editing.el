(defun act-on-line-or-region (function)
  (funcall function
           (if mark-active (region-beginning) (line-beginning-position))
           (if mark-active (region-end) (line-end-position))))

(defun comment-or-uncomment-line-or-region nil
  (interactive)
  (act-on-line-or-region 'comment-or-uncomment-region))

(defun end-of-line-or-last-not-blank (&optional n)
  (interactive)
  (if (= (point) (save-excursion (end-of-line n) (point)))
      (skip-syntax-backward "-")
    (end-of-line n)))

(defun back-to-indentation-or-beginning (&optional n)
  (interactive)
  (if (= (point) (save-excursion (back-to-indentation) (point)))
      (beginning-of-line) 
    (beginning-of-line n) (back-to-indentation)))

(defun indent-line-or-region nil
  (interactive)
  (act-on-line-or-region 'indent-region))

(defun indent-buffer nil 
  "Indent whole buffer"
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max))))

(defun backward-line (count)
  (interactive "p")
  (forward-line (* -1 count)))

(defun join-following-line nil
  (interactive)
  (join-line 1))

(defun zap-upto-char (arg char)
  (interactive "p\ncZap up to char: ")
  (zap-to-char arg char)
  (insert char)
  (backward-char))

(def-keys (current-global-map)
  ;; command remapping
  [remap move-beginning-of-line] 'back-to-indentation-or-beginning
  [remap move-end-of-line] 'end-of-line-or-last-not-blank

  ;; util
  "M-n" 'forward-page
  "M-p" 'backward-page
  
  "H-J" 'join-following-line
  "H-z" 'zap-upto-char
  
  ;; vi-like movement
  "H-j" 'next-line
  "H-k" 'previous-line
  "H-h" 'backward-char
  "H-l" 'forward-char
  "C-H-j" 'forward-paragraph
  "C-H-k" 'backward-paragraph
  "C-H-l" 'forward-word
  "C-H-h" 'backward-word
 
  ;; commenting
  "H-;" 'comment-or-uncomment-line-or-region
  "H-\\" 'indent-line-or-region
  "H-|" 'indent-buffer

  ;; transposing
  "H-t c" 'transpose-chars
  "H-t w" 'transpose-words
  "H-t x" 'transpose-sexps
  "H-t s" 'transpose-sentenses
  "H-t p" 'transpose-paragraphs
  "H-t l" 'transpose-lines
  
  ) ;; def-keys

;; from http://blog.zenspider.com/archives/2007/02/new_category_emacs.html
(define-key isearch-mode-map (kbd "C-o")
  (lambda ()
    (interactive)
    (let ((case-fold-search isearch-case-fold-search))
      (occur (if isearch-regexp isearch-string
               (regexp-quote isearch-string))))))

(provide 'vee/editing)
