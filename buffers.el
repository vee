(defun other-window-reverse (arg &optional all-frames)
  (interactive "p")
  (other-window (* -1 arg) all-frames))

(defun kill-other-buffer-and-window (arg &optional all-frames)
  (interactive "p\nP")
  (save-excursion 
    (other-window arg all-frames)
    (kill-buffer-and-window-noprompt)))

(defun kill-buffer-and-window-noprompt ()
  "Kill the current buffer and delete the selected window."
  (interactive)
  (kill-this-buffer)
  (delete-window (selected-window)))

(defun find-file-and-kill-current-buffer () 
  "Find file and kill the current buffer in the same window"
  (interactive)
  (let ((buffer (current-buffer)))
    (call-interactively (or (command-remapping 'find-file) 'find-file))
    (kill-buffer buffer)))

(defun kill-buffers-matching (regexp)
  "Kill all the buffers matching regexp"
  (interactive "BKill buffers matching: ")
  (mapc  '(lambda (buffer) 
            (and (string-match regexp (buffer-name buffer))
                 (kill-buffer buffer)))
         (buffer-list)))

(defun create-scratch-buffer nil
  (set-buffer (get-buffer-create "*scratch*"))
  (save-window-excursion
    (require 'help-fns)
    (random t)
    (let* ((commands (loop for s being the symbols
                           when (commandp s) collect s))
           (command (nth (random (length commands)) commands))
           (help-xref-following t)
           (begin nil)
           (title (format "| Emacs random command: `%s' |" command)))


      (describe-function command)

      (lisp-interaction-mode)

      (goto-char (point-min))
      (insert initial-scratch-message)
      (setq begin (point-marker))

      (insert-char ?+ (string-width title))
      (insert "\n" title "\n")
      (insert-char ?+ (string-width title))
      (insert "\nInvoke using: " (with-temp-buffer (where-is command t) 
                                                   (buffer-string))
              "\n\n")
      (goto-char (point-max))

      (insert "\n\n" 
              "++++++++++++++++++\n"
              "| Random Fortune |\n"
              "++++++++++++++++++\n\n"
              (shell-command-to-string 
               (concat "which fortune 2>/dev/null 1>/dev/null "
                       "&& fortune futurama"
                       "|| echo You have no fortune!")))

      (insert "\n")
    
      (comment-region begin (point)))
    (buffer-enable-undo)
    (make-local-variable 'kill-buffer-query-functions)
    (add-hook 'kill-buffer-query-functions 'kill-scratch-buffer)))

(defun kill-scratch-buffer nil
  (set-buffer (get-buffer "*scratch*"))
  (remove-hook 'kill-buffer-query-functions 'kill-scratch-buffer)
  (kill-buffer (current-buffer))
  (create-scratch-buffer)
  nil)

(add-hook 'emacs-startup-hook 'create-scratch-buffer)

(def-keys (current-global-map)
  ;; windows
  "C-x p" 'other-window-reverse ;; just like `ojkther-window'

  ;; frames
  "H-f 0" 'delete-frame
  "H-f 2" 'new-frame
  "H-f 1" 'delete-other-frames
  "H-f o" 'other-frame
  
  ;; killing buffers
  "C-x 9 f" 'find-file-and-kill-current-buffer
  "C-x 9 0" 'kill-buffer-and-window-noprompt
  "C-x 9 1" 'kill-other-buffer-and-window
  "C-x 9 k" 'kill-buffers-matching)

(when (require? 'wn-mode) (enable-mode wn))

(provide 'vee/buffers)
