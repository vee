;; del.icio.us
;; delicious-el is at the same directory as vee's root
(eval-after-load 'delicioapi
  `(delicious-secrets "v.i.c"
                      (setq delicious-api-user user
                            delicious-api-password pass
                            delicious-api-from from)
                      (delicious-api-register-auth)))

(eval-after-load 'w3m
  '(or (require? 'delicious)
       (let ((d (expand-file-name "../delicious-el" 
                                  (get 'vee 'root-dir))))
         (and (file-directory-p d) (add-to-list 'load-path d)
              (require 'delicious)))))

(provide 'vee/www)