(require 'cl)

(defun require? (feature)
  (or (featurep feature)
      (condition-case err (require (if (stringp feature) 
				       (intern feature) feature))
	(error 
	 (lwarn :require? :warning "[require %s]: %s" feature err)
	 nil))
      (condition-case err (load-library (format "%s" feature))
	(error
	 (lwarn :require? :warning "[load-library %s]: %s" feature err)
	 nil))))

;;; FIXME potencial bug, use cl-macs functions to get keyword arguments
(defun funcall! (function &rest args)
  "`funcall' FUNCTION. Keywords: :may-require :or-value"
  (let (may-require or-value)
    (mapc (lambda (key-var)
            (let ((key-tail (memq (car key-var) args)))
              (when key-tail
                (setq args (reverse (set-difference args 
                                                    (subseq key-tail 0 2))))
                (set (cadr key-var) (cadr key-tail)))))
          '((:may-require may-require) (:or-value or-value)))
    (or (and (or (fboundp function)
                 (and may-require
                      (require? may-require)
                      (fboundp function)))
             (apply function args))
        or-value)))

(defun autoload-iff (function file &optional docstring interactive type)
  "set autoload if file found. FILE has found."
  (and (symbolp file) (setq file (symbol-name file)))
  (and (locate-library file)
       (autoload function file docstring interactive type)))

(defmacro enable-mode (&rest modes)
  `(mapc (lambda (m) (funcall m 1))
	 ',(mapcar (lambda (m) (intern (format "%s-mode" m))) modes)))

(defmacro disable-mode (&rest modes)
  `(mapc (lambda (m) (funcall m -1))
	 ',(mapcar (lambda (m) (intern (format "%s-mode" m))) modes)))

(defun def-keys (keymap &rest bindings)
  "Define key-bindings in KEYMAP."
  (let (key fun)
     (while bindings
       (setq key (pop bindings))
       (setq fun (pop bindings))
       (define-key keymap (if (stringp key) (read-kbd-macro key) key)fun))))

(defmacro defsecrets (name file)
  "Define a wrapper around NAME secrets defined in FILE.
A variable named NAME must be defined in FILE with the same form as `let*'s 
VARLIST.

FILE can be either an string or a symbol to be required."
  `(defmacro ,name (name &rest forms)
     ,(format "`let*' wrapper for `%s' variable" name)
     (require? ,file)
     (let ((info (assoc (eval name) ,name)))
       (if info `(let* ((name ,(car info)) ,@(cdr info)) ,@forms)
         `(lwarn name :warning "No secrets for %s" ,name) nil))))

(provide 'vee/core-fun)
