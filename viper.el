(setq-default ;; viper-always nil
              viper-expert-level '5
              viper-ex-style-editing nil
              viper-ex-style-motion nil
              viper-auto-indent t
              viper-inhibit-startup-message t
              viper-eletric-mode t
              viper-want-ctl-h-help t
              viper-want-emacs-keys-in-insert t
              viper-want-emacs-keys-in-vi t
              viper-vi-style-in-minibuffer nil
              viper-no-multiple-ESC t
              viper-case-fold-search t
              viper-re-search t
              viper-re-query-replace t
              viper-syntax-preference 'emacs
              viper-delete-backwards-in-replace t
              viper-parse-sexp-ignore-comments nil
              viper-ESC-moves-cursor-back nil)

(custom-set-variables '(viper-toggle-key [(control escape)]))

(defface viper-insert-face nil "Viper insert face")

(defun viper-add-mode-to-state (mode state)
  (let* ((state-alist '((vi      viper-vi-state-mode-list)
                        (emacs   viper-emacs-state-mode-list)
                        (insert  viper-insert-state-mode-list)))
         (state-mode-list (assoc state state-alist)))
    (add-to-list (cadr state-mode-list) mode)
    (mapcar (lambda (x) 
              (set (cadr x) (remove mode (symbol-value (cadr x)))))
            (remove state-mode-list state-alist))))

(put 'viper-add-local-keys 'lisp-indent-function 1)

(and (fboundp 'viper-buffer-search-enable)
     (viper-buffer-search-enable))

(set 'viper-read-buffer-function 
     (cond 
      ((featurep 'ido) 'ido-read-buffer)
      ((featurep 'iswitchb) 'iswitchb-read-buffer)
      (t 'read-buffer)))

;; FIXME
(and window-system
     (setq viper-minibuffer-insert-face 'viper-insert-face
           viper-vi-state-cursor-color "cyan")) 

(add-hook 'viper-load-hook 'viper-define-aditional-bindings)

(defun viper-esc-command-lambda (command)
  `(lambda nil (interactive)
     (call-interactively (cond ((eq viper-current-state 'vi-state)
                                  'viper-ESC)
                                 ((eq viper-current-state 'insert-state)
                                  'viper-exit-insert-state)
                                 ((eq viper-current-state 'replace-state)
                                  'viper-replace-state-exit-cmd)
                                 (t 'viper-change-state-to-vi)))
     (call-interactively ',(or (and (symbolp command)
                                    (command-remapping command)) command))))

(defun viper-define-aditional-bindings nil
  (define-key viper-vi-global-user-map [(meta ?#)] 'comment-or-uncomment-line-or-region)
  (define-key viper-vi-global-user-map [(control ??)] 'help)
  (define-key viper-vi-global-user-map "q" 'kill-buffer)
  (define-key viper-vi-global-user-map "\M-[" 'backward-paragraph)
  (define-key viper-vi-global-user-map "\M-]" 'forward-paragraph)  
  (define-key viper-vi-global-user-map "\C-f" 'forward-char)
  (define-key viper-vi-global-user-map "\C-b" 'backward-char)
  (define-key viper-vi-global-user-map "\M-v" 'scroll-down)
  (define-key viper-vi-global-user-map "\C-v" 'scroll-up)
  (define-key viper-vi-global-user-map "\C-y" 'yank)
  (define-key viper-vi-global-user-map "\C-h" 'backward-word)
  (define-key viper-vi-global-user-map "\C-l" 'forward-word)
  ;;(define-key viper-vi-global-user-map " " 'scroll-up)
  ;;(define-key viper-vi-global-user-map [(backspace)] 'scroll-down)
  (define-key viper-vi-global-user-map "\C-j" 'forward-paragraph)
  (define-key viper-vi-global-user-map "\C-k" 'backward-paragraph)
  (define-key viper-vi-global-user-map "\C-xp"
    (lambda nil (interactive) (other-window -1)))
  (define-key viper-vi-global-user-map "\C-e" 
    (or (command-remapping 'move-end-of-line) 'move-end-of-line))
  (define-key viper-insert-global-user-map "\C-xo"
    (viper-esc-command-lambda 'other-window))
  (define-key viper-insert-global-user-map "\C-xp"
    (viper-esc-command-lambda (lambda nil (interactive) (other-window -1))))
  (define-key viper-insert-global-user-map [(control return)]
    (viper-esc-command-lambda 'viper-nil))
  (define-key viper-insert-global-user-map "\C-x\C-s" 
    (viper-esc-command-lambda 'save-buffer))
  (define-key viper-insert-global-user-map "\C-g"
    (viper-esc-command-lambda 'keyboard-quit))
  (define-key viper-insert-global-user-map "\C-]"
    (viper-esc-command-lambda 'abort-recursive-edit))
  (define-key viper-insert-global-user-map "\C-x\C-f"
    (viper-esc-command-lambda 'find-file)))

(defun viper-show-current-state nil
  (unless (window-minibuffer-p)
    (message "%s %s %s"
             (let ((str (buffer-name nil)))
               (add-text-properties 
                0 (length str)
                (list 'face
                      (cond ((eq viper-current-state 'vi-state) 
                             'minibuffer-prompt)
                            ((eq viper-current-state 'insert-state)
                             'viper-insert-face)
                            ((eq viper-current-state 'replace-state)
                             'viper-replace-overlay-face)
                            ((eq viper-current-state 'emacs-state)
                             'mode-line)))
                str)
               str)
             mode-name
             (cond ((eq viper-current-state 'vi-state)
                    (if window-system 
                        #("[ € v ı Ł ]"
                          0  1  (face widget-inactive-face)
                          2  9  (face fringe)
                          4  7  (face highlight)
                          10 11 (face widget-inactive-face))
                      "[ E v i L ]"))
                   ((eq viper-current-state 'insert-state)
                    (if window-system
                        #("[ ı n § ]"
                          0 1 (face widget-inactive-face)
                          2 7 (face viper-insert-face)
                          8 9 (face widget-inactive-face))
                      "[ I N S ]"))
                   ((eq viper-current-state 'replace-state)
                    (if window-system 
                        #("[ r æ þ ]"
                          0 1 (face widget-inactive-face)
                          2 7 (face query-replace)
                          8 9 (face widget-inactive-face))
                      "[ R E P ]"))
                   ((eq viper-current-state 'emacs-state)
                    (if window-system   
                        #("[ € µ æ ¢ § ]"
                          0  1  (face widget-inactive-face)
                          2  11 (face secondary-selection)
                          12 13 (face widget-inactive-face))
                      "[ E M A C S ]"))))))

(add-hook 'viper-vi-state-hook 'viper-show-current-state)
(add-hook 'viper-insert-state-hook 'viper-show-current-state)
(add-hook 'viper-replace-state-hook 'viper-show-current-state)
(add-hook 'viper-emacs-state-hook 'viper-show-current-state)

(provide 'dot-viper)
