(require 'gnus)
;; for debugging see `gnus-verbose' and `nnimap-debug'

;;;; 
;; Main
;;;;
(defadvice netrc-parse (around netrc-parse-inline first (file) activate)
  (if (stringp file)
      ad-do-it
    (setq ad-return-value file)))

(unless (functionp 'gnus-browse-imaps-server)
  (defun gnus-browse-imaps-server (server)
    "Browse a mail server in Gnus via IMAP-SSL."
    (interactive "sServer name: ")
    (gnus-group-browse-foreign-server
     (list 'nnimap server
           `((nnimap-address ,server)
             (nnimap-stream ssl)
             ;;'(nnimap-list-pattern ("INBOX" "mail/*" "Mail/*" "INBOX.*"))
             (nnimap-expunge-on-close ask))))))
  
(defun gnus-group-quit! nil
  (interactive)
  (let ((noninteractive t)) (gnus-group-quit)))
(add-hook 'kill-emacs-hook 'gnus-group-quit!)

(defun gnus-group-killed! nil
  (and (equal (buffer-name) "*Group*") 
       (remove-hook 'kill-buffer-hook 'gnus-group-killed!) (gnus-group-quit!)))
(add-hook 'kill-buffer-hook 'gnus-group-killed!)

(defun gnus-user-format-function-d (headers)
  (let ((time (gnus-group-timestamp gnus-tmp-group)))
    (if time
        (format-time-string "%b %d  %H:%M" time)
      "")))

(setq gnus-select-method '(nnnil))
(setq gnus-secondary-select-methods nil)

;;  gmane news/mail gateway
(add-to-list 'gnus-secondary-select-methods '(nntp "news.gmane.org"))

;;  slashdot
;;(add-to-list 'gnus-secondary-select-methods '(nnslashdot "")) ;; FIX 

;; imap accounts
(mapc (lambda (s) 
        (gnus-secrets s 
                      (add-to-list 'gnus-secondary-select-methods 
                                   `(nnimap ,name
                                            (nnimap-address ,server)
                                            (nnimap-authinfo-file 
                                             ((("machine" . ,server)
                                               ("login" . ,user)
                                               ("password" . ,pass))))
                                            (nnimap-stream ssl)
                                            (nnimap-expunge-on-close ask)))))
      '("ls" "hk" "th"))

;; simple mail accounts
(eval-after-load "mail-source" '(require 'tgnus-pop3))
(add-to-list 'gnus-secondary-select-methods '(nnml ""))
(setq mail-sources nil)
(gnus-secrets "gmail"
              (add-to-list 'mail-sources
                           `(pop :user ,user :password ,pass
                                 :connection ssl :leave t
                                 :server ,server :port 995)))

;; Posting styles
(setq gnus-posting-styles
      '(("^nnml.*gmail"
         (name "Victor Hugo Borja")
         (addres "vicborj@gmail.com"))
        (".*" (name "Victor Hugo Borja")
         (address "vic@rubyforge.org"))))


;;;;
;; The Group Buffer
;;;;

;; auto subscribe
;; (add-to-list 'gnus-options-subscribe '("gmane.*ruby")) ;; I love ruby
;; (add-to-list 'gnus-option-not-subscribe '("microsoft")) ;; aint matters

;; set a fancy *Groups* buffer
;; (setq gnus-group-line-format "%M%S%p%P%5y:%B%(%g%)%l %O\n") ;; default
(setq gnus-group-uncollapsed-levels 3
      gnus-group-line-format
      "%M\%S\%p\%P\%5y: %(%-40,40c%) %ud\n")

;; group highlight high/low scores
;;(add-to-list 'gnus-group-highlight '((nil) . face))

;; when entering a group go to the highest-scored unread article
(setq gnus-auto-select-first 'best)

;; most read groups get higher scores `gnus(Group Score)'
(add-hook 'gnus-summary-exit-hook 'gnus-summary-bubble-group)

;; fixed group properties (Use `G p' on *Group*) or setq gnus-parameters

;; use topic mode by default
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

;; keep track of when a group was last read
(add-hook 'gnus-select-group-hook 'gnus-group-set-timestamp)


;; Topics
;; from [emacswiki: GnusFormatting]
(setq gnus-topic-line-format "%i[ %u&topic-line; ] %v\n")
    
;; this corresponds to a topic line format of "%n %A"
(defun gnus-user-format-function-topic-line (dummy)
  (let ((topic-face (if (zerop total-number-of-articles)
                        'gnus-summary-low-ancient
                      'gnus-summary-high-ancient)))
    (propertize
     (format "%s %d" name total-number-of-articles)
     'face topic-face)))


;;;;
;; The Summary Buffer
;;;;

;; article line format `gnus(Summary Buffer Lines)'
;; (setq gnus-summary-line-format "%U%R%z%I%(%[%4L: %-23,23f%]%) %s\n") ;; def

(setq gnus-summary-gather-subject-limit 'fuzzy)

(def-keys gnus-summary-mode-map "$" 'gnus-summary-mark-as-spam)


(eval-after-load 'nnrss 
  '(progn 
     
     ;; Browse the rss entry URL with C-RET at *Summary buffer
     (require 'browse-url)

     (defun browse-nnrss-url (arg)
       (interactive "p")
       (let ((url (assq nnrss-url-field
                        (mail-header-extra
                         (gnus-data-header
                          (assq (gnus-summary-article-number)
                                gnus-newsgroup-data))))))
         (if url
             (browse-url (cdr url))
           (gnus-summary-scroll-up arg))))
     
     (add-hook 'gnus-summary-mode-hook
               (lambda nil
                 (def-keys gnus-summary-mode-map
                   "H-<return>" 'browse-nnrss-url ;; browse-url-function
                   "M-<return>" (lambda nil (interactive) ;; konqueror
                                  (let ((browse-url-browser-function
                                         'browse-url-kde))
                                    (call-interactively 'browse-nnrss-url)))
                   "C-<return>" (lambda nil (interactive) ;; w3m
                                  (let ((browse-url-browser-function
                                         (lambda (url &rest ignored)
                                           (require 'w3m)
                                           (w3m-browse-url url t))))
                                    (call-interactively 'browse-nnrss-url))))))

     (add-to-list 'nnmail-extra-headers nnrss-url-field)

     (add-hook 'gnus-summary-mode-hook
               (lambda () ;; from  http://anirudhs.chaosnet.org/blog/2004.07.13.html
                 (when (string-match "^nnrss:.*" gnus-newsgroup-name)
                   (make-local-variable 'gnus-show-threads)
                   (make-local-variable 'gnus-article-sort-functions)
                   (make-local-variable 'gnus-use-adaptive-scoring)
                   (make-local-variable 'gnus-use-scoring)
                   (make-local-variable 'gnus-score-find-score-files-function)
                   (make-local-variable 'gnus-summary-line-format)
                   (setq gnus-show-threads nil)
                   (setq gnus-article-sort-functions 'gnus-article-sort-by-date)
                   (setq gnus-use-adaptive-scoring nil)
                   (setq gnus-use-scoring t)
                   (setq gnus-score-find-score-files-function 'gnus-score-find-single)
                   (setq gnus-summary-line-format "%U%R%z%d %I%(%[ %s %]%)\n")
                   )))))

(provide 'vee/gnus)
