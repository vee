(setq vee/color-theme 'vee/color-theme-dark-vee)

(mwheel-install)

(add-to-list 'default-frame-alist '(cursor-type . (bar . 3)))
(add-to-list 'default-frame-alist '(font . "6x13"))

(when (and vee/color-theme (require? 'color-theme))
  (funcall! vee/color-theme :may-require vee/color-theme))

(setq frame-title-format "emacs - %b")

(provide 'vee/windowed)
