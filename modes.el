(defvar sheebang-modes-alist
  '(("^#!.*ruby" . ruby-mode)
    ("^#!.*perl" . perl-mode)
    ("^#!.*\\(bash\\|sh\\)" . shell-script-mode))
  "Alist of sheebang (REGEXP . MAJOR-MODE).
When a file's major mode cannot be determined using auto-mode-alist, vee tries
to guess the correct mode from the sheebang line.")

(defun guess-mode-using-sheebang (buffer)
  (save-excursion
    (save-match-data
      (switch-to-buffer buffer)
      (goto-char 0)
      (cdr (assoc-if (lambda (s) (looking-at s)) sheebang-modes-alist)))))

(defun may-set-mode-using-sheebang nil
  (when (eq major-mode default-major-mode)
    (let ((mode (guess-mode-using-sheebang (current-buffer))))
      (and mode (funcall mode 1)))))

(add-hook 'find-file-hook 'may-set-mode-using-sheebang)

;; html / xml => nxml
(when (require? '80nxml-mode-gentoo)
   (add-to-list 'auto-mode-alist
                (cons (concat "\\." (regexp-opt '("xml" "xsd" 
                                                  "sch" "rng" 
                                                  "xslt" "svg" 
                                                  "rss") t) "\\'")
                      'nxml-mode)))

;; css
(when (require? '50css-mode-gentoo)
  (eval-after-load 'css-mode
    (setq cssm-indent-function 'cssm-c-style-indenter
          cssm-indent-level '2)))

;; java / jsp

;; perl5
(defalias 'perl-mode 'cperl-mode)
(defalias 'cperl-invalid-face 'trailing-whitespace)

;; ruby
(when (require? '50ruby-mode-gentoo)
  (add-to-list 'auto-mode-alist 
               '("\\([rR]ake\\(file\\)?\\|\\.rake\\)\\'" . ruby-mode))
  (eval-after-load 'ruby-mode
    ;; check if using el4r, and enable some cool stuff.
    nil))

;; rails


;; org mode
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(eval-after-load 'org '(require 'vee/org))

;; haskell
(and (locate-library "50haskell-mode-gentoo")
     (load-library "50haskell-mode-gentoo"))

(provide 'vee/modes)