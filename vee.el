;; Vic's Emacs Environment
;;

;; This is the main file to be loaded by .emacs
;; (add-to-list 'load-path "/path/to/vee/lib/lisp")
;; (require 'vee)
;;

;;;_* Code:

;;;_ , determine environment
(put 'vee 'lisp-dir (file-name-directory (locate-library "vee")))
(put 'vee 'root-dir (expand-file-name ".." (get 'vee 'lisp-dir)))
(put 'vee 'data-dir (expand-file-name "data" (get 'vee 'root-dir)))
(put 'vee 'site-lisp (expand-file-name "site-lisp" (get 'vee 'root-dir)))

(add-to-list 'load-path (get 'vee 'site-lisp))

(and (not (featurep 'warnings))
          (require 'warnings)
          (setq warning-minimum-level :error))

;;;_ , core and essential customizations
(require 'vee/core-fun)

(require 'vee/essential)

(require 'vee/modes)

(require 'vee/editing)

(require 'vee/buffers)

;;_ , windowed environment
(when window-system 
  (require 'vee/windowed)
  
  (and (setq-default viper-mode nil)
       (setq-default viper-always t)
       (setq-default viper-custom-file-name
                     (expand-file-name "vee/viper.el" (get 'vee 'lisp-dir)))
       (require 'viper)))

;;_ , gnus
(defsecrets gnus-secrets 'vee/secrets)
(setq-default gnus-init-file 
              (expand-file-name "vee/gnus.el" (get 'vee 'lisp-dir)))
(def-keys (current-global-map) "H-g" (kbd "C-1 <<gnus>>") "H-G" 'gnus)

;;_ , www
(and (locate-library "70emacs-w3m-gentoo") (load-library "70emacs-w3m-gentoo"))
(defsecrets delicious-secrets 'vee/secrets)
(eval-after-load 'browse-url '(require 'vee/www))


;;;_ , provide
(provide 'vee)
