(enable-mode mouse-avoidance column-number line-number global-font-lock
	     transient-mark display-time)

(disable-mode menu-bar)
(when (boundp 'tool-bar-mode) (disable-mode tool-bar))
(when (boundp 'scroll-bar-mode) (disable-mode scroll-bar))

(if (require? 'mic-paren) (paren-activate) (enable-mode show-paren))

(when display-time-mode 
  (setq display-time-day-and-date nil
	display-time-24hr-format  nil))

(setq inhibit-startup-message t
      echo-keystrokes 0.1)

(require? 'session)
(when (require? 'elscreen)
  (setq-default elscreen-display-tab nil))

(when (require? 'icomplete) (icomplete-mode))
;;(add-to-list 'load-path (expand-file-name "icicles" (get 'vee 'site-lisp)))
;;(when (require? 'icicles) (icy-mode))
(when (require?  'modeline-posn) (size-indication-mode 1))
(ido-mode 1)
  

(setq ;; from emacs-wiki/BackupDirectory
 backup-by-copying-when-linked 1
 backup-by-copying-when-mismatch 1
 backup-by-copying t
 backup-directory-alist '(("." . "~/.emacs.d/saves"))
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)

(setq-default ;; override some defaults
 indicate-empty-lines 1
 show-trailing-whitespace nil
 indent-tabs-mode nil)

(put 'narrow-to-region 'disabled nil) ;; C-x n n
(put 'narrow-to-page 'disabled nil) ;; C-x n p
(put 'erase-buffer 'disabled nil)
(put 'overwrite-mode 'disabled 1) ;; I hate overwrite mode

;; disable various keys to avoid temptation
;; (global-unset-key [\left])
;; (global-unset-key [\right])
;; (global-unset-key [\up])
;; (global-unset-key [\down])
;; (global-unset-key [\home])
;; (global-unset-key [\end])
;; (global-unset-key [\next])
;; (global-unset-key [\prior])

(provide 'vee/essential)